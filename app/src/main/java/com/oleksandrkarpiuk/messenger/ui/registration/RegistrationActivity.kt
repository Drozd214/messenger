package com.oleksandrkarpiuk.messenger.ui.registration

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.oleksandrkarpiuk.messenger.R
import com.oleksandrkarpiuk.messenger.data.repositories.auth.AuthRepositoryImpl
import com.oleksandrkarpiuk.messenger.data.repositories.users.UsersRepositoryImpl
import com.oleksandrkarpiuk.messenger.ui.login.LoginActivity
import com.oleksandrkarpiuk.messenger.ui.main.MainActivity
import com.oleksandrkarpiuk.messenger.utils.errors.RegistrationErrors
import com.oleksandrkarpiuk.messenger.utils.providers.StringProvider
import com.oleksandrkarpiuk.messenger.utils.validators.UserValidator
import kotlinx.android.synthetic.main.activity_registration.*

class RegistrationActivity : AppCompatActivity(),
    RegistrationContract.View {


    //TODO DI with Koin
    private val stringProvider = StringProvider(this)
    private val authRepository = AuthRepositoryImpl()
    private val usersRepository = UsersRepositoryImpl()
    private val userValidator = UserValidator()
    private val registrationErrors = RegistrationErrors()
    private val presenter = RegistrationPresenter(
        this,
        stringProvider,
        authRepository,
        usersRepository,
        userValidator,
        registrationErrors)




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)

        presenter.onCreate()

        init()
    }


    private fun init() {
        initButtons()
    }


    private fun initButtons() {
        registerBtn.setOnClickListener {
            presenter.onRegisterBtnClicked()
        }

        haveAccountTv.setOnClickListener {
            presenter.onHaveAccountClicked()
        }
    }


    override fun getNameContent(): String {
        return nameEt.text.toString()
    }


    override fun getSurnameContent(): String {
        return surnameEt.text.toString()
    }


    override fun getEmailContent(): String {
        return emailEt.text.toString()
    }

    override fun getPasswordContent(): String {
        return passwordEt.text.toString()
    }


    override fun getConfirmedPasswordContent(): String {
        return confirmedPasswordEt.text.toString()
    }


    override fun showShortToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }


    override fun launchLoginActivity() {
        startActivity(LoginActivity.newIntent(this))
    }


    override fun launchMainActivity() {
        startActivity(MainActivity.newIntent(this))
    }


}