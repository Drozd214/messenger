package com.oleksandrkarpiuk.messenger.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.oleksandrkarpiuk.messenger.R
import com.oleksandrkarpiuk.messenger.data.repositories.auth.AuthRepositoryImpl
import com.oleksandrkarpiuk.messenger.ui.main.MainActivity
import com.oleksandrkarpiuk.messenger.ui.main.MainContract
import com.oleksandrkarpiuk.messenger.utils.errors.RegistrationErrors
import com.oleksandrkarpiuk.messenger.utils.providers.StringProvider
import com.oleksandrkarpiuk.messenger.utils.validators.UserValidator
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginContract.View {


    private val stringProvider = StringProvider(this)
    private val authRepository = AuthRepositoryImpl()
    private val userValidator = UserValidator()
    private val registrationErrors = RegistrationErrors()
    private val presenter = LoginPresenter(
        this,
        stringProvider,
        authRepository,
        userValidator,
        registrationErrors)




    companion object {
        fun newIntent(context: Context) : Intent {
            return Intent(context, LoginActivity::class.java)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        init()
    }


    private fun init() {
        initButtons()
    }


    private fun initButtons() {
        signInBtn.setOnClickListener {
            presenter.onSignInBtnClicked()
        }

        haveAccountTv.setOnClickListener {
            presenter.onHaveAccountClicked()
        }
    }


    override fun getEmailContent(): String {
        return emailEt.text.toString()
    }


    override fun getPasswordContent(): String {
        return passwordEt.text.toString()
    }


    override fun showShortToast(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }


    override fun finish() {
        super.finish()
    }


    override fun launchMainActivity() {
        startActivity(MainActivity.newIntent(this))
    }


}