package com.oleksandrkarpiuk.messenger.ui.login

interface LoginContract {


    interface View {
        fun getEmailContent() : String

        fun getPasswordContent() : String

        fun showShortToast(message: String)

        fun finish()

        fun launchMainActivity()
    }


    interface ActionListener {
        fun onSignInBtnClicked()

        fun onHaveAccountClicked()
    }


}