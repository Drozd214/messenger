package com.oleksandrkarpiuk.messenger.ui.registration

import com.google.firebase.auth.FirebaseAuthException
import com.oleksandrkarpiuk.messenger.R
import com.oleksandrkarpiuk.messenger.data.repositories.auth.AuthRepository
import com.oleksandrkarpiuk.messenger.data.repositories.users.UsersRepository
import com.oleksandrkarpiuk.messenger.data.repositories.utills.Listener
import com.oleksandrkarpiuk.messenger.models.User
import com.oleksandrkarpiuk.messenger.utils.errors.GeneralErrors
import com.oleksandrkarpiuk.messenger.utils.errors.RegistrationErrors
import com.oleksandrkarpiuk.messenger.utils.providers.StringProvider
import com.oleksandrkarpiuk.messenger.utils.validators.EmailValidator
import com.oleksandrkarpiuk.messenger.utils.validators.PasswordValidator
import com.oleksandrkarpiuk.messenger.utils.validators.UserValidator

class RegistrationPresenter(
    private val view: RegistrationContract.View,
    private val stringProvider: StringProvider,
    private val authRepository: AuthRepository,
    private val usersRepository: UsersRepository,
    private val userValidator: UserValidator,
    private val registrationErrors: RegistrationErrors
) : RegistrationContract.ActionListener {


    override fun onCreate() {
        if(authRepository.isCurrentUserExist()) {
            view.launchMainActivity()
        }
    }


    override fun onRegisterBtnClicked() {
        val name = view.getNameContent()
        val surname = view.getSurnameContent()
        val email = view.getEmailContent()
        val password = view.getPasswordContent()
        val confirmedPassword = view.getConfirmedPasswordContent()

        if(isFieldsValidate(name, surname, email, password, confirmedPassword)) {
            //TODO підтвердження листа після реєстрації і потім відкривати логування
            authRepository.registerUserWithEmailAndPassword(email, password, object: Listener<Unit> {
                override fun onSuccess(data: Unit) {
                    addUserToDatabase(name, surname, email, password)
                    authRepository.signOut()

                    with(view) {
                        showShortToast(stringProvider.getString(R.string.success))
                        launchLoginActivity()
                    }
                }

                override fun onFailure(exception: Exception) {
                    if(exception is FirebaseAuthException) {
                        view.showShortToast(stringProvider.getString(registrationErrors.detectRegisterError(exception).errorStringId))
                    } else {
                        view.showShortToast(stringProvider.getString(GeneralErrors.getErrorMessageId(exception)))
                    }
                }
            })
        }
    }


    private fun isFieldsValidate(
        name: String,
        surname: String,
        email: String,
        password: String,
        confirmedPassword: String
    ) : Boolean {

        if(
            name.isEmpty() ||
            surname.isEmpty() ||
            email.isEmpty() ||
            password.isEmpty() ||
            confirmedPassword.isEmpty()
        ) {
            view.showShortToast(stringProvider.getString(R.string.fields_is_empty_hint))
            return false
        }

        if(!isPasswordCorrect(password, confirmedPassword)) { return false }

        if(!isEmailCorrect(email)) { return false }

        return true
    }


    private fun isPasswordCorrect(password: String, confirmedPassword: String) : Boolean {
        if(password != confirmedPassword) {
            view.showShortToast(stringProvider.getString(R.string.passwords_are_not_equal_hint))
            return false
        }

        return when(userValidator.passwordValidator.isPasswordValid(password)) {
            PasswordValidator.PasswordValidationResult.TOO_SHORT ->
            {
                view.showShortToast(stringProvider.getString(R.string.password_are_short_hint))
                false
            }

            PasswordValidator.PasswordValidationResult.SIMPLE ->
            {
                view.showShortToast(stringProvider.getString(R.string.password_are_simple_hint))
                false
            }

            PasswordValidator.PasswordValidationResult.SUCCESS ->
            {
                return true
            }
        }
    }


    private fun isEmailCorrect(email: String) : Boolean {
        return when(userValidator.emailValidator.isEmailValid(email)) {
            EmailValidator.EmailValidationResult.BADLY_FORMAT ->
            {
                view.showShortToast(stringProvider.getString(R.string.badly_email_format_hint))
                false
            }

            EmailValidator.EmailValidationResult.SUCCESS ->
            {
                true
            }
        }
    }


    private fun addUserToDatabase(name: String, surname: String,
                                  email: String, password: String) {
        usersRepository.addNewUser(
            User(
                id = authRepository.getCurrentUserId()!!,
                name = name,
                surname = surname,
                email = email,
                password = password
            ), object: Listener<Unit> {
                override fun onSuccess(data: Unit) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onFailure(exception: Exception) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

            })
    }


    override fun onHaveAccountClicked() {
        view.launchLoginActivity()
    }


}