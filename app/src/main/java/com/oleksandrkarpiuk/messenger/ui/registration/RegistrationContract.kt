package com.oleksandrkarpiuk.messenger.ui.registration

interface RegistrationContract {


    interface View {
        fun getNameContent() : String

        fun getSurnameContent() : String

        fun getEmailContent() : String

        fun getPasswordContent() : String

        fun getConfirmedPasswordContent() : String

        fun showShortToast(message: String)

        fun launchLoginActivity()

        fun launchMainActivity()
    }


    interface ActionListener {
        fun onCreate()

        fun onRegisterBtnClicked()

        fun onHaveAccountClicked()
    }


}