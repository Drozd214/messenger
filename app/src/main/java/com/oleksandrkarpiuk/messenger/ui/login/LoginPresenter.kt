package com.oleksandrkarpiuk.messenger.ui.login

import com.google.firebase.auth.FirebaseAuthException
import com.oleksandrkarpiuk.messenger.R
import com.oleksandrkarpiuk.messenger.data.repositories.auth.AuthRepository
import com.oleksandrkarpiuk.messenger.data.repositories.utills.Listener
import com.oleksandrkarpiuk.messenger.utils.errors.GeneralErrors
import com.oleksandrkarpiuk.messenger.utils.errors.RegistrationErrors
import com.oleksandrkarpiuk.messenger.utils.providers.StringProvider
import com.oleksandrkarpiuk.messenger.utils.validators.EmailValidator
import com.oleksandrkarpiuk.messenger.utils.validators.UserValidator

class LoginPresenter(
    private val view: LoginContract.View,
    private val stringProvider: StringProvider,
    private val authRepository: AuthRepository,
    private val userValidator: UserValidator,
    private val registrationErrors: RegistrationErrors
) : LoginContract.ActionListener {


    override fun onSignInBtnClicked() {
        val email = view.getEmailContent()
        val password = view.getPasswordContent()

        if(isFieldsValidate(email, password)) {
            authRepository.signInUserWithEmailAndPassword(email, password, object: Listener<Unit> {
                override fun onSuccess(data: Unit) {
                    view.launchMainActivity()
                }

                override fun onFailure(exception: Exception) {
                    if(exception is FirebaseAuthException) {
                        view.showShortToast(stringProvider.getString(registrationErrors.detectRegisterError(exception).errorStringId))
                    } else {
                        view.showShortToast(stringProvider.getString(GeneralErrors.getErrorMessageId(exception)))
                    }
                }
            })
        }
    }


    private fun isFieldsValidate(email: String, password: String) : Boolean {
        if(email.isEmpty() || password.isEmpty()) {
            view.showShortToast(stringProvider.getString(R.string.fields_is_empty_hint))
            return false
        }

        if(!isEmailValid(email)) { return false }

        return true
    }


    private fun isEmailValid(email: String) : Boolean {
        return when(userValidator.emailValidator.isEmailValid(email)) {
            EmailValidator.EmailValidationResult.BADLY_FORMAT -> {
                view.showShortToast(stringProvider.getString(R.string.badly_email_format_hint))
                false
            }

            EmailValidator.EmailValidationResult.SUCCESS -> {
                true
            }
        }
    }


    override fun onHaveAccountClicked() {
        view.finish()
    }


}