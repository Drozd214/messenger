package com.oleksandrkarpiuk.messenger.data.repositories.utills

interface Listener<Data> {

    fun onSuccess(data: Data)

    fun onFailure(exception: Exception)

}