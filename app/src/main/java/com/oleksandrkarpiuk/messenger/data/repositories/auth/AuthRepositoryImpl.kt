package com.oleksandrkarpiuk.messenger.data.repositories.auth

import com.google.firebase.auth.FirebaseAuth
import com.oleksandrkarpiuk.messenger.data.repositories.utills.Listener
import com.oleksandrkarpiuk.messenger.utils.error

class AuthRepositoryImpl : AuthRepository {


    private val authInstance = FirebaseAuth.getInstance()




    override fun registerUserWithEmailAndPassword(
        email: String,
        password: String,
        listener: Listener<Unit>
    ) {
        authInstance.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener {task ->
                if(task.isSuccessful) {
                    listener.onSuccess(Unit)
                } else {
                    listener.onFailure(task.error)
                }
            }
    }


    override fun signInUserWithEmailAndPassword(
        email: String,
        password: String,
        listener: Listener<Unit>
    ) {
        authInstance.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener { task ->
                if(task.isSuccessful) {
                    listener.onSuccess(Unit)
                } else {
                    listener.onFailure(task.error)
                }
            }
    }


    override fun getCurrentUserId(): String? {
        return if(isCurrentUserExist()) {
            authInstance.uid.toString()
        } else {
            null
        }
    }


    override fun isCurrentUserExist(): Boolean {
        return authInstance.currentUser != null
    }


    override fun signOut() {
        authInstance.signOut()
    }


}