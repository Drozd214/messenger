package com.oleksandrkarpiuk.messenger.data.repositories.users

import com.google.firebase.database.FirebaseDatabase
import com.oleksandrkarpiuk.messenger.data.repositories.utills.Listener
import com.oleksandrkarpiuk.messenger.models.User
import com.oleksandrkarpiuk.messenger.utils.error

class UsersRepositoryImpl : UsersRepository {


    private val usersInstance = FirebaseDatabase.getInstance().getReference("/users")




    override fun addNewUser(user: User, listener: Listener<Unit>) {
        usersInstance.child(user.id).setValue(user)
            .addOnCompleteListener { task ->
                if(task.isSuccessful)
                {

                } else {
                    listener.onFailure(task.error)
                }
            }
    }


}