package com.oleksandrkarpiuk.messenger.data.repositories.auth

import com.oleksandrkarpiuk.messenger.data.repositories.utills.Listener

interface AuthRepository {

    fun registerUserWithEmailAndPassword(email: String, password: String, listener: Listener<Unit>)

    fun signInUserWithEmailAndPassword(email: String, password: String, listener: Listener<Unit>)

    fun getCurrentUserId() : String?

    fun isCurrentUserExist() : Boolean

    fun signOut()

}