package com.oleksandrkarpiuk.messenger.data.repositories.users

import com.oleksandrkarpiuk.messenger.data.repositories.utills.Listener
import com.oleksandrkarpiuk.messenger.models.User

interface UsersRepository {

    fun addNewUser(user: User, listener: Listener<Unit>)

}