package com.oleksandrkarpiuk.messenger.utils.errors

import com.google.firebase.FirebaseTooManyRequestsException
import com.oleksandrkarpiuk.messenger.R
import java.lang.Exception

class GeneralErrors {


    companion object {
        fun getErrorMessageId(exception: Exception) : Int {
            return when(exception) {
                is FirebaseTooManyRequestsException -> R.string.too_many_request_error
                else -> R.string.default_error
            }
        }
    }

}