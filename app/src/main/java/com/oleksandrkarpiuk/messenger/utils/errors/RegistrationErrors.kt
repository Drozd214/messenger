package com.oleksandrkarpiuk.messenger.utils.errors

import androidx.annotation.StringRes
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.oleksandrkarpiuk.messenger.R
import java.lang.Exception

class RegistrationErrors {


    enum class RegistrationErrorResult(@StringRes val errorStringId: Int) {
        USER_COLLISION(R.string.email_already_taken_error),
        INVALID_CREDENTIAL(R.string.invalid_password_error),
        INVALID_USER(R.string.email_not_registered_error),
        UNKNOWN(0)
    }


    fun detectRegisterError(exception: Exception) : RegistrationErrorResult {
        return when(exception) {
            is FirebaseAuthUserCollisionException -> RegistrationErrorResult.USER_COLLISION
            is FirebaseAuthInvalidCredentialsException -> RegistrationErrorResult.INVALID_CREDENTIAL
            is FirebaseAuthInvalidUserException -> RegistrationErrorResult.INVALID_USER
            else -> RegistrationErrorResult.UNKNOWN
        }
    }


}