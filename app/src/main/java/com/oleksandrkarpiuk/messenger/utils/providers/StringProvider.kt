package com.oleksandrkarpiuk.messenger.utils.providers

import android.content.Context
import androidx.annotation.StringRes

class StringProvider (private val context: Context) {


    fun getString(@StringRes id: Int) : String {
        return context.getString(id)
    }


}