package com.oleksandrkarpiuk.messenger.utils.validators

class EmailValidator {


    enum class EmailValidationResult {
        SUCCESS,
        BADLY_FORMAT,
    }


    fun isEmailValid(email: String) : EmailValidationResult {
        if(email.contains('@') and email.contains('.')) {
            if(email.indexOf('@') > email.lastIndexOf('.')) {
                return EmailValidationResult.BADLY_FORMAT
            }
        }

        return EmailValidationResult.SUCCESS
    }


}