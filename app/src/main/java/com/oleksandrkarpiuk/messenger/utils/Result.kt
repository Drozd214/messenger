package com.oleksandrkarpiuk.messenger.utils

sealed class Result {


    class Success<out T>(
        val value: T
    ) : Result() {

        override fun toString(): String {
            return value.toString()
        }

    }


    class Failure(
        val exception: Throwable
    ) : Result() {

        override fun toString(): String {
            return exception.toString()
        }

    }


}