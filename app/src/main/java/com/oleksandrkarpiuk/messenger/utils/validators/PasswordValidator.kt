package com.oleksandrkarpiuk.messenger.utils.validators

class PasswordValidator {


    enum class PasswordValidationResult {
        SUCCESS,
        TOO_SHORT,
        SIMPLE
    }


    fun isPasswordValid(password: String) : PasswordValidationResult {
        if(password.length < 8) {
            return PasswordValidationResult.TOO_SHORT
        }

        return PasswordValidationResult.SUCCESS
    }


}