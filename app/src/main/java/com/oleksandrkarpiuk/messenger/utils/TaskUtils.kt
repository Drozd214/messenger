package com.oleksandrkarpiuk.messenger.utils

import com.google.android.gms.tasks.Task

//TODO розібратись з generics
val <T> Task<T>.error : Exception
    get() = exception ?: Exception()